package org.ishafoundation.pincodeservice.web.rest;

import org.ishafoundation.pincodeservice.PincodeserviceApp;

import org.ishafoundation.pincodeservice.domain.Postoffice;
import org.ishafoundation.pincodeservice.repository.PostofficeRepository;
import org.ishafoundation.pincodeservice.service.PostofficeService;
import org.ishafoundation.pincodeservice.service.dto.PostofficeDTO;
import org.ishafoundation.pincodeservice.service.mapper.PostofficeMapper;
import org.ishafoundation.pincodeservice.web.rest.errors.ExceptionTranslator;
import org.ishafoundation.pincodeservice.service.dto.PostofficeCriteria;
import org.ishafoundation.pincodeservice.service.PostofficeQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static org.ishafoundation.pincodeservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PostofficeResource REST controller.
 *
 * @see PostofficeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PincodeserviceApp.class)
public class PostofficeResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_BRANCH_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_BRANCH_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_DELIVERY_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_DELIVERY_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_TALUK = "AAAAAAAAAA";
    private static final String UPDATED_TALUK = "BBBBBBBBBB";

    private static final String DEFAULT_CIRCLE = "AAAAAAAAAA";
    private static final String UPDATED_CIRCLE = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_DIVISION = "AAAAAAAAAA";
    private static final String UPDATED_DIVISION = "BBBBBBBBBB";

    private static final String DEFAULT_REGION = "AAAAAAAAAA";
    private static final String UPDATED_REGION = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final Integer DEFAULT_PINCODE = 1;
    private static final Integer UPDATED_PINCODE = 2;

    @Autowired
    private PostofficeRepository postofficeRepository;

    @Autowired
    private PostofficeMapper postofficeMapper;
    
    @Autowired
    private PostofficeService postofficeService;

    @Autowired
    private PostofficeQueryService postofficeQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPostofficeMockMvc;

    private Postoffice postoffice;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PostofficeResource postofficeResource = new PostofficeResource(postofficeService, postofficeQueryService);
        this.restPostofficeMockMvc = MockMvcBuilders.standaloneSetup(postofficeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Postoffice createEntity(EntityManager em) {
        Postoffice postoffice = new Postoffice()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .branchType(DEFAULT_BRANCH_TYPE)
            .deliveryStatus(DEFAULT_DELIVERY_STATUS)
            .taluk(DEFAULT_TALUK)
            .circle(DEFAULT_CIRCLE)
            .district(DEFAULT_DISTRICT)
            .division(DEFAULT_DIVISION)
            .region(DEFAULT_REGION)
            .state(DEFAULT_STATE)
            .country(DEFAULT_COUNTRY)
            .pincode(DEFAULT_PINCODE);
        return postoffice;
    }

    @Before
    public void initTest() {
        postoffice = createEntity(em);
    }

    @Test
    @Transactional
    public void createPostoffice() throws Exception {
        int databaseSizeBeforeCreate = postofficeRepository.findAll().size();

        // Create the Postoffice
        PostofficeDTO postofficeDTO = postofficeMapper.toDto(postoffice);
        restPostofficeMockMvc.perform(post("/api/postoffices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(postofficeDTO)))
            .andExpect(status().isCreated());

        // Validate the Postoffice in the database
        List<Postoffice> postofficeList = postofficeRepository.findAll();
        assertThat(postofficeList).hasSize(databaseSizeBeforeCreate + 1);
        Postoffice testPostoffice = postofficeList.get(postofficeList.size() - 1);
        assertThat(testPostoffice.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPostoffice.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPostoffice.getBranchType()).isEqualTo(DEFAULT_BRANCH_TYPE);
        assertThat(testPostoffice.getDeliveryStatus()).isEqualTo(DEFAULT_DELIVERY_STATUS);
        assertThat(testPostoffice.getTaluk()).isEqualTo(DEFAULT_TALUK);
        assertThat(testPostoffice.getCircle()).isEqualTo(DEFAULT_CIRCLE);
        assertThat(testPostoffice.getDistrict()).isEqualTo(DEFAULT_DISTRICT);
        assertThat(testPostoffice.getDivision()).isEqualTo(DEFAULT_DIVISION);
        assertThat(testPostoffice.getRegion()).isEqualTo(DEFAULT_REGION);
        assertThat(testPostoffice.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testPostoffice.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testPostoffice.getPincode()).isEqualTo(DEFAULT_PINCODE);
    }

    @Test
    @Transactional
    public void createPostofficeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = postofficeRepository.findAll().size();

        // Create the Postoffice with an existing ID
        postoffice.setId(1L);
        PostofficeDTO postofficeDTO = postofficeMapper.toDto(postoffice);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPostofficeMockMvc.perform(post("/api/postoffices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(postofficeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Postoffice in the database
        List<Postoffice> postofficeList = postofficeRepository.findAll();
        assertThat(postofficeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPostoffices() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList
        restPostofficeMockMvc.perform(get("/api/postoffices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(postoffice.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].branchType").value(hasItem(DEFAULT_BRANCH_TYPE.toString())))
            .andExpect(jsonPath("$.[*].deliveryStatus").value(hasItem(DEFAULT_DELIVERY_STATUS.toString())))
            .andExpect(jsonPath("$.[*].taluk").value(hasItem(DEFAULT_TALUK.toString())))
            .andExpect(jsonPath("$.[*].circle").value(hasItem(DEFAULT_CIRCLE.toString())))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].division").value(hasItem(DEFAULT_DIVISION.toString())))
            .andExpect(jsonPath("$.[*].region").value(hasItem(DEFAULT_REGION.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].pincode").value(hasItem(DEFAULT_PINCODE)));
    }
    
    @Test
    @Transactional
    public void getPostoffice() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get the postoffice
        restPostofficeMockMvc.perform(get("/api/postoffices/{id}", postoffice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(postoffice.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.branchType").value(DEFAULT_BRANCH_TYPE.toString()))
            .andExpect(jsonPath("$.deliveryStatus").value(DEFAULT_DELIVERY_STATUS.toString()))
            .andExpect(jsonPath("$.taluk").value(DEFAULT_TALUK.toString()))
            .andExpect(jsonPath("$.circle").value(DEFAULT_CIRCLE.toString()))
            .andExpect(jsonPath("$.district").value(DEFAULT_DISTRICT.toString()))
            .andExpect(jsonPath("$.division").value(DEFAULT_DIVISION.toString()))
            .andExpect(jsonPath("$.region").value(DEFAULT_REGION.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.pincode").value(DEFAULT_PINCODE));
    }

    @Test
    @Transactional
    public void getAllPostofficesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where name equals to DEFAULT_NAME
        defaultPostofficeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the postofficeList where name equals to UPDATED_NAME
        defaultPostofficeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPostofficesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultPostofficeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the postofficeList where name equals to UPDATED_NAME
        defaultPostofficeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPostofficesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where name is not null
        defaultPostofficeShouldBeFound("name.specified=true");

        // Get all the postofficeList where name is null
        defaultPostofficeShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where description equals to DEFAULT_DESCRIPTION
        defaultPostofficeShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the postofficeList where description equals to UPDATED_DESCRIPTION
        defaultPostofficeShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPostofficesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultPostofficeShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the postofficeList where description equals to UPDATED_DESCRIPTION
        defaultPostofficeShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllPostofficesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where description is not null
        defaultPostofficeShouldBeFound("description.specified=true");

        // Get all the postofficeList where description is null
        defaultPostofficeShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByBranchTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where branchType equals to DEFAULT_BRANCH_TYPE
        defaultPostofficeShouldBeFound("branchType.equals=" + DEFAULT_BRANCH_TYPE);

        // Get all the postofficeList where branchType equals to UPDATED_BRANCH_TYPE
        defaultPostofficeShouldNotBeFound("branchType.equals=" + UPDATED_BRANCH_TYPE);
    }

    @Test
    @Transactional
    public void getAllPostofficesByBranchTypeIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where branchType in DEFAULT_BRANCH_TYPE or UPDATED_BRANCH_TYPE
        defaultPostofficeShouldBeFound("branchType.in=" + DEFAULT_BRANCH_TYPE + "," + UPDATED_BRANCH_TYPE);

        // Get all the postofficeList where branchType equals to UPDATED_BRANCH_TYPE
        defaultPostofficeShouldNotBeFound("branchType.in=" + UPDATED_BRANCH_TYPE);
    }

    @Test
    @Transactional
    public void getAllPostofficesByBranchTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where branchType is not null
        defaultPostofficeShouldBeFound("branchType.specified=true");

        // Get all the postofficeList where branchType is null
        defaultPostofficeShouldNotBeFound("branchType.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByDeliveryStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where deliveryStatus equals to DEFAULT_DELIVERY_STATUS
        defaultPostofficeShouldBeFound("deliveryStatus.equals=" + DEFAULT_DELIVERY_STATUS);

        // Get all the postofficeList where deliveryStatus equals to UPDATED_DELIVERY_STATUS
        defaultPostofficeShouldNotBeFound("deliveryStatus.equals=" + UPDATED_DELIVERY_STATUS);
    }

    @Test
    @Transactional
    public void getAllPostofficesByDeliveryStatusIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where deliveryStatus in DEFAULT_DELIVERY_STATUS or UPDATED_DELIVERY_STATUS
        defaultPostofficeShouldBeFound("deliveryStatus.in=" + DEFAULT_DELIVERY_STATUS + "," + UPDATED_DELIVERY_STATUS);

        // Get all the postofficeList where deliveryStatus equals to UPDATED_DELIVERY_STATUS
        defaultPostofficeShouldNotBeFound("deliveryStatus.in=" + UPDATED_DELIVERY_STATUS);
    }

    @Test
    @Transactional
    public void getAllPostofficesByDeliveryStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where deliveryStatus is not null
        defaultPostofficeShouldBeFound("deliveryStatus.specified=true");

        // Get all the postofficeList where deliveryStatus is null
        defaultPostofficeShouldNotBeFound("deliveryStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByTalukIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where taluk equals to DEFAULT_TALUK
        defaultPostofficeShouldBeFound("taluk.equals=" + DEFAULT_TALUK);

        // Get all the postofficeList where taluk equals to UPDATED_TALUK
        defaultPostofficeShouldNotBeFound("taluk.equals=" + UPDATED_TALUK);
    }

    @Test
    @Transactional
    public void getAllPostofficesByTalukIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where taluk in DEFAULT_TALUK or UPDATED_TALUK
        defaultPostofficeShouldBeFound("taluk.in=" + DEFAULT_TALUK + "," + UPDATED_TALUK);

        // Get all the postofficeList where taluk equals to UPDATED_TALUK
        defaultPostofficeShouldNotBeFound("taluk.in=" + UPDATED_TALUK);
    }

    @Test
    @Transactional
    public void getAllPostofficesByTalukIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where taluk is not null
        defaultPostofficeShouldBeFound("taluk.specified=true");

        // Get all the postofficeList where taluk is null
        defaultPostofficeShouldNotBeFound("taluk.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByCircleIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where circle equals to DEFAULT_CIRCLE
        defaultPostofficeShouldBeFound("circle.equals=" + DEFAULT_CIRCLE);

        // Get all the postofficeList where circle equals to UPDATED_CIRCLE
        defaultPostofficeShouldNotBeFound("circle.equals=" + UPDATED_CIRCLE);
    }

    @Test
    @Transactional
    public void getAllPostofficesByCircleIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where circle in DEFAULT_CIRCLE or UPDATED_CIRCLE
        defaultPostofficeShouldBeFound("circle.in=" + DEFAULT_CIRCLE + "," + UPDATED_CIRCLE);

        // Get all the postofficeList where circle equals to UPDATED_CIRCLE
        defaultPostofficeShouldNotBeFound("circle.in=" + UPDATED_CIRCLE);
    }

    @Test
    @Transactional
    public void getAllPostofficesByCircleIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where circle is not null
        defaultPostofficeShouldBeFound("circle.specified=true");

        // Get all the postofficeList where circle is null
        defaultPostofficeShouldNotBeFound("circle.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByDistrictIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where district equals to DEFAULT_DISTRICT
        defaultPostofficeShouldBeFound("district.equals=" + DEFAULT_DISTRICT);

        // Get all the postofficeList where district equals to UPDATED_DISTRICT
        defaultPostofficeShouldNotBeFound("district.equals=" + UPDATED_DISTRICT);
    }

    @Test
    @Transactional
    public void getAllPostofficesByDistrictIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where district in DEFAULT_DISTRICT or UPDATED_DISTRICT
        defaultPostofficeShouldBeFound("district.in=" + DEFAULT_DISTRICT + "," + UPDATED_DISTRICT);

        // Get all the postofficeList where district equals to UPDATED_DISTRICT
        defaultPostofficeShouldNotBeFound("district.in=" + UPDATED_DISTRICT);
    }

    @Test
    @Transactional
    public void getAllPostofficesByDistrictIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where district is not null
        defaultPostofficeShouldBeFound("district.specified=true");

        // Get all the postofficeList where district is null
        defaultPostofficeShouldNotBeFound("district.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByDivisionIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where division equals to DEFAULT_DIVISION
        defaultPostofficeShouldBeFound("division.equals=" + DEFAULT_DIVISION);

        // Get all the postofficeList where division equals to UPDATED_DIVISION
        defaultPostofficeShouldNotBeFound("division.equals=" + UPDATED_DIVISION);
    }

    @Test
    @Transactional
    public void getAllPostofficesByDivisionIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where division in DEFAULT_DIVISION or UPDATED_DIVISION
        defaultPostofficeShouldBeFound("division.in=" + DEFAULT_DIVISION + "," + UPDATED_DIVISION);

        // Get all the postofficeList where division equals to UPDATED_DIVISION
        defaultPostofficeShouldNotBeFound("division.in=" + UPDATED_DIVISION);
    }

    @Test
    @Transactional
    public void getAllPostofficesByDivisionIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where division is not null
        defaultPostofficeShouldBeFound("division.specified=true");

        // Get all the postofficeList where division is null
        defaultPostofficeShouldNotBeFound("division.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByRegionIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where region equals to DEFAULT_REGION
        defaultPostofficeShouldBeFound("region.equals=" + DEFAULT_REGION);

        // Get all the postofficeList where region equals to UPDATED_REGION
        defaultPostofficeShouldNotBeFound("region.equals=" + UPDATED_REGION);
    }

    @Test
    @Transactional
    public void getAllPostofficesByRegionIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where region in DEFAULT_REGION or UPDATED_REGION
        defaultPostofficeShouldBeFound("region.in=" + DEFAULT_REGION + "," + UPDATED_REGION);

        // Get all the postofficeList where region equals to UPDATED_REGION
        defaultPostofficeShouldNotBeFound("region.in=" + UPDATED_REGION);
    }

    @Test
    @Transactional
    public void getAllPostofficesByRegionIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where region is not null
        defaultPostofficeShouldBeFound("region.specified=true");

        // Get all the postofficeList where region is null
        defaultPostofficeShouldNotBeFound("region.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where state equals to DEFAULT_STATE
        defaultPostofficeShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the postofficeList where state equals to UPDATED_STATE
        defaultPostofficeShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllPostofficesByStateIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where state in DEFAULT_STATE or UPDATED_STATE
        defaultPostofficeShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the postofficeList where state equals to UPDATED_STATE
        defaultPostofficeShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllPostofficesByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where state is not null
        defaultPostofficeShouldBeFound("state.specified=true");

        // Get all the postofficeList where state is null
        defaultPostofficeShouldNotBeFound("state.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where country equals to DEFAULT_COUNTRY
        defaultPostofficeShouldBeFound("country.equals=" + DEFAULT_COUNTRY);

        // Get all the postofficeList where country equals to UPDATED_COUNTRY
        defaultPostofficeShouldNotBeFound("country.equals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllPostofficesByCountryIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where country in DEFAULT_COUNTRY or UPDATED_COUNTRY
        defaultPostofficeShouldBeFound("country.in=" + DEFAULT_COUNTRY + "," + UPDATED_COUNTRY);

        // Get all the postofficeList where country equals to UPDATED_COUNTRY
        defaultPostofficeShouldNotBeFound("country.in=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllPostofficesByCountryIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where country is not null
        defaultPostofficeShouldBeFound("country.specified=true");

        // Get all the postofficeList where country is null
        defaultPostofficeShouldNotBeFound("country.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByPincodeIsEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where pincode equals to DEFAULT_PINCODE
        defaultPostofficeShouldBeFound("pincode.equals=" + DEFAULT_PINCODE);

        // Get all the postofficeList where pincode equals to UPDATED_PINCODE
        defaultPostofficeShouldNotBeFound("pincode.equals=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllPostofficesByPincodeIsInShouldWork() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where pincode in DEFAULT_PINCODE or UPDATED_PINCODE
        defaultPostofficeShouldBeFound("pincode.in=" + DEFAULT_PINCODE + "," + UPDATED_PINCODE);

        // Get all the postofficeList where pincode equals to UPDATED_PINCODE
        defaultPostofficeShouldNotBeFound("pincode.in=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllPostofficesByPincodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where pincode is not null
        defaultPostofficeShouldBeFound("pincode.specified=true");

        // Get all the postofficeList where pincode is null
        defaultPostofficeShouldNotBeFound("pincode.specified=false");
    }

    @Test
    @Transactional
    public void getAllPostofficesByPincodeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where pincode greater than or equals to DEFAULT_PINCODE
        defaultPostofficeShouldBeFound("pincode.greaterOrEqualThan=" + DEFAULT_PINCODE);

        // Get all the postofficeList where pincode greater than or equals to UPDATED_PINCODE
        defaultPostofficeShouldNotBeFound("pincode.greaterOrEqualThan=" + UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void getAllPostofficesByPincodeIsLessThanSomething() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        // Get all the postofficeList where pincode less than or equals to DEFAULT_PINCODE
        defaultPostofficeShouldNotBeFound("pincode.lessThan=" + DEFAULT_PINCODE);

        // Get all the postofficeList where pincode less than or equals to UPDATED_PINCODE
        defaultPostofficeShouldBeFound("pincode.lessThan=" + UPDATED_PINCODE);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultPostofficeShouldBeFound(String filter) throws Exception {
        restPostofficeMockMvc.perform(get("/api/postoffices?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(postoffice.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].branchType").value(hasItem(DEFAULT_BRANCH_TYPE.toString())))
            .andExpect(jsonPath("$.[*].deliveryStatus").value(hasItem(DEFAULT_DELIVERY_STATUS.toString())))
            .andExpect(jsonPath("$.[*].taluk").value(hasItem(DEFAULT_TALUK.toString())))
            .andExpect(jsonPath("$.[*].circle").value(hasItem(DEFAULT_CIRCLE.toString())))
            .andExpect(jsonPath("$.[*].district").value(hasItem(DEFAULT_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].division").value(hasItem(DEFAULT_DIVISION.toString())))
            .andExpect(jsonPath("$.[*].region").value(hasItem(DEFAULT_REGION.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].pincode").value(hasItem(DEFAULT_PINCODE)));

        // Check, that the count call also returns 1
        restPostofficeMockMvc.perform(get("/api/postoffices/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultPostofficeShouldNotBeFound(String filter) throws Exception {
        restPostofficeMockMvc.perform(get("/api/postoffices?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPostofficeMockMvc.perform(get("/api/postoffices/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPostoffice() throws Exception {
        // Get the postoffice
        restPostofficeMockMvc.perform(get("/api/postoffices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePostoffice() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        int databaseSizeBeforeUpdate = postofficeRepository.findAll().size();

        // Update the postoffice
        Postoffice updatedPostoffice = postofficeRepository.findById(postoffice.getId()).get();
        // Disconnect from session so that the updates on updatedPostoffice are not directly saved in db
        em.detach(updatedPostoffice);
        updatedPostoffice
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .branchType(UPDATED_BRANCH_TYPE)
            .deliveryStatus(UPDATED_DELIVERY_STATUS)
            .taluk(UPDATED_TALUK)
            .circle(UPDATED_CIRCLE)
            .district(UPDATED_DISTRICT)
            .division(UPDATED_DIVISION)
            .region(UPDATED_REGION)
            .state(UPDATED_STATE)
            .country(UPDATED_COUNTRY)
            .pincode(UPDATED_PINCODE);
        PostofficeDTO postofficeDTO = postofficeMapper.toDto(updatedPostoffice);

        restPostofficeMockMvc.perform(put("/api/postoffices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(postofficeDTO)))
            .andExpect(status().isOk());

        // Validate the Postoffice in the database
        List<Postoffice> postofficeList = postofficeRepository.findAll();
        assertThat(postofficeList).hasSize(databaseSizeBeforeUpdate);
        Postoffice testPostoffice = postofficeList.get(postofficeList.size() - 1);
        assertThat(testPostoffice.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPostoffice.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPostoffice.getBranchType()).isEqualTo(UPDATED_BRANCH_TYPE);
        assertThat(testPostoffice.getDeliveryStatus()).isEqualTo(UPDATED_DELIVERY_STATUS);
        assertThat(testPostoffice.getTaluk()).isEqualTo(UPDATED_TALUK);
        assertThat(testPostoffice.getCircle()).isEqualTo(UPDATED_CIRCLE);
        assertThat(testPostoffice.getDistrict()).isEqualTo(UPDATED_DISTRICT);
        assertThat(testPostoffice.getDivision()).isEqualTo(UPDATED_DIVISION);
        assertThat(testPostoffice.getRegion()).isEqualTo(UPDATED_REGION);
        assertThat(testPostoffice.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testPostoffice.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testPostoffice.getPincode()).isEqualTo(UPDATED_PINCODE);
    }

    @Test
    @Transactional
    public void updateNonExistingPostoffice() throws Exception {
        int databaseSizeBeforeUpdate = postofficeRepository.findAll().size();

        // Create the Postoffice
        PostofficeDTO postofficeDTO = postofficeMapper.toDto(postoffice);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPostofficeMockMvc.perform(put("/api/postoffices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(postofficeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Postoffice in the database
        List<Postoffice> postofficeList = postofficeRepository.findAll();
        assertThat(postofficeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePostoffice() throws Exception {
        // Initialize the database
        postofficeRepository.saveAndFlush(postoffice);

        int databaseSizeBeforeDelete = postofficeRepository.findAll().size();

        // Get the postoffice
        restPostofficeMockMvc.perform(delete("/api/postoffices/{id}", postoffice.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Postoffice> postofficeList = postofficeRepository.findAll();
        assertThat(postofficeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Postoffice.class);
        Postoffice postoffice1 = new Postoffice();
        postoffice1.setId(1L);
        Postoffice postoffice2 = new Postoffice();
        postoffice2.setId(postoffice1.getId());
        assertThat(postoffice1).isEqualTo(postoffice2);
        postoffice2.setId(2L);
        assertThat(postoffice1).isNotEqualTo(postoffice2);
        postoffice1.setId(null);
        assertThat(postoffice1).isNotEqualTo(postoffice2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PostofficeDTO.class);
        PostofficeDTO postofficeDTO1 = new PostofficeDTO();
        postofficeDTO1.setId(1L);
        PostofficeDTO postofficeDTO2 = new PostofficeDTO();
        assertThat(postofficeDTO1).isNotEqualTo(postofficeDTO2);
        postofficeDTO2.setId(postofficeDTO1.getId());
        assertThat(postofficeDTO1).isEqualTo(postofficeDTO2);
        postofficeDTO2.setId(2L);
        assertThat(postofficeDTO1).isNotEqualTo(postofficeDTO2);
        postofficeDTO1.setId(null);
        assertThat(postofficeDTO1).isNotEqualTo(postofficeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(postofficeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(postofficeMapper.fromId(null)).isNull();
    }
}
