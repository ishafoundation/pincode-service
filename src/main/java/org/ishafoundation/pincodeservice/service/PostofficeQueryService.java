package org.ishafoundation.pincodeservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import org.ishafoundation.pincodeservice.domain.Postoffice;
import org.ishafoundation.pincodeservice.domain.*; // for static metamodels
import org.ishafoundation.pincodeservice.repository.PostofficeRepository;
import org.ishafoundation.pincodeservice.service.dto.PostofficeCriteria;
import org.ishafoundation.pincodeservice.service.dto.PostofficeDTO;
import org.ishafoundation.pincodeservice.service.mapper.PostofficeMapper;

/**
 * Service for executing complex queries for Postoffice entities in the database.
 * The main input is a {@link PostofficeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PostofficeDTO} or a {@link Page} of {@link PostofficeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PostofficeQueryService extends QueryService<Postoffice> {

    private final Logger log = LoggerFactory.getLogger(PostofficeQueryService.class);

    private final PostofficeRepository postofficeRepository;

    private final PostofficeMapper postofficeMapper;

    public PostofficeQueryService(PostofficeRepository postofficeRepository, PostofficeMapper postofficeMapper) {
        this.postofficeRepository = postofficeRepository;
        this.postofficeMapper = postofficeMapper;
    }

    /**
     * Return a {@link List} of {@link PostofficeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PostofficeDTO> findByCriteria(PostofficeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Postoffice> specification = createSpecification(criteria);
        return postofficeMapper.toDto(postofficeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PostofficeDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PostofficeDTO> findByCriteria(PostofficeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Postoffice> specification = createSpecification(criteria);
        return postofficeRepository.findAll(specification, page)
            .map(postofficeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PostofficeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Postoffice> specification = createSpecification(criteria);
        return postofficeRepository.count(specification);
    }

    /**
     * Function to convert PostofficeCriteria to a {@link Specification}
     */
    private Specification<Postoffice> createSpecification(PostofficeCriteria criteria) {
        Specification<Postoffice> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Postoffice_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Postoffice_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Postoffice_.description));
            }
            if (criteria.getBranchType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBranchType(), Postoffice_.branchType));
            }
            if (criteria.getDeliveryStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDeliveryStatus(), Postoffice_.deliveryStatus));
            }
            if (criteria.getTaluk() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTaluk(), Postoffice_.taluk));
            }
            if (criteria.getCircle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCircle(), Postoffice_.circle));
            }
            if (criteria.getDistrict() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDistrict(), Postoffice_.district));
            }
            if (criteria.getDivision() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDivision(), Postoffice_.division));
            }
            if (criteria.getRegion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegion(), Postoffice_.region));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), Postoffice_.state));
            }
            if (criteria.getCountry() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountry(), Postoffice_.country));
            }
            if (criteria.getPincode() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPincode(), Postoffice_.pincode));
            }
        }
        return specification;
    }
}
