package org.ishafoundation.pincodeservice.service.impl;

import org.ishafoundation.pincodeservice.service.PostofficeService;
import org.ishafoundation.pincodeservice.domain.Postoffice;
import org.ishafoundation.pincodeservice.repository.PostofficeRepository;
import org.ishafoundation.pincodeservice.service.dto.PostofficeDTO;
import org.ishafoundation.pincodeservice.service.mapper.PostofficeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Postoffice.
 */
@Service
@Transactional
public class PostofficeServiceImpl implements PostofficeService {

    private final Logger log = LoggerFactory.getLogger(PostofficeServiceImpl.class);

    private final PostofficeRepository postofficeRepository;

    private final PostofficeMapper postofficeMapper;

    public PostofficeServiceImpl(PostofficeRepository postofficeRepository, PostofficeMapper postofficeMapper) {
        this.postofficeRepository = postofficeRepository;
        this.postofficeMapper = postofficeMapper;
    }

    /**
     * Save a postoffice.
     *
     * @param postofficeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PostofficeDTO save(PostofficeDTO postofficeDTO) {
        log.debug("Request to save Postoffice : {}", postofficeDTO);

        Postoffice postoffice = postofficeMapper.toEntity(postofficeDTO);
        postoffice = postofficeRepository.save(postoffice);
        return postofficeMapper.toDto(postoffice);
    }

    /**
     * Get all the postoffices.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PostofficeDTO> findAll() {
        log.debug("Request to get all Postoffices");
        return postofficeRepository.findAll().stream()
            .map(postofficeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one postoffice by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PostofficeDTO> findOne(Long id) {
        log.debug("Request to get Postoffice : {}", id);
        return postofficeRepository.findById(id)
            .map(postofficeMapper::toDto);
    }

    /**
     * Delete the postoffice by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Postoffice : {}", id);
        postofficeRepository.deleteById(id);
    }
}
