package org.ishafoundation.pincodeservice.service;

import org.ishafoundation.pincodeservice.service.dto.PostofficeDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Postoffice.
 */
public interface PostofficeService {

    /**
     * Save a postoffice.
     *
     * @param postofficeDTO the entity to save
     * @return the persisted entity
     */
    PostofficeDTO save(PostofficeDTO postofficeDTO);

    /**
     * Get all the postoffices.
     *
     * @return the list of entities
     */
    List<PostofficeDTO> findAll();


    /**
     * Get the "id" postoffice.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PostofficeDTO> findOne(Long id);

    /**
     * Delete the "id" postoffice.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
