package org.ishafoundation.pincodeservice.service.mapper;

import org.ishafoundation.pincodeservice.domain.*;
import org.ishafoundation.pincodeservice.service.dto.PostofficeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Postoffice and its DTO PostofficeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PostofficeMapper extends EntityMapper<PostofficeDTO, Postoffice> {



    default Postoffice fromId(Long id) {
        if (id == null) {
            return null;
        }
        Postoffice postoffice = new Postoffice();
        postoffice.setId(id);
        return postoffice;
    }
}
