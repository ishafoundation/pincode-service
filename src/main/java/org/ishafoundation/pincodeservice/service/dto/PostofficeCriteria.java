package org.ishafoundation.pincodeservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the Postoffice entity. This class is used in PostofficeResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /postoffices?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PostofficeCriteria implements Serializable {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter description;

    private StringFilter branchType;

    private StringFilter deliveryStatus;

    private StringFilter taluk;

    private StringFilter circle;

    private StringFilter district;

    private StringFilter division;

    private StringFilter region;

    private StringFilter state;

    private StringFilter country;

    private IntegerFilter pincode;

    public PostofficeCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public StringFilter getBranchType() {
        return branchType;
    }

    public void setBranchType(StringFilter branchType) {
        this.branchType = branchType;
    }

    public StringFilter getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(StringFilter deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public StringFilter getTaluk() {
        return taluk;
    }

    public void setTaluk(StringFilter taluk) {
        this.taluk = taluk;
    }

    public StringFilter getCircle() {
        return circle;
    }

    public void setCircle(StringFilter circle) {
        this.circle = circle;
    }

    public StringFilter getDistrict() {
        return district;
    }

    public void setDistrict(StringFilter district) {
        this.district = district;
    }

    public StringFilter getDivision() {
        return division;
    }

    public void setDivision(StringFilter division) {
        this.division = division;
    }

    public StringFilter getRegion() {
        return region;
    }

    public void setRegion(StringFilter region) {
        this.region = region;
    }

    public StringFilter getState() {
        return state;
    }

    public void setState(StringFilter state) {
        this.state = state;
    }

    public StringFilter getCountry() {
        return country;
    }

    public void setCountry(StringFilter country) {
        this.country = country;
    }

    public IntegerFilter getPincode() {
        return pincode;
    }

    public void setPincode(IntegerFilter pincode) {
        this.pincode = pincode;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PostofficeCriteria that = (PostofficeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(description, that.description) &&
            Objects.equals(branchType, that.branchType) &&
            Objects.equals(deliveryStatus, that.deliveryStatus) &&
            Objects.equals(taluk, that.taluk) &&
            Objects.equals(circle, that.circle) &&
            Objects.equals(district, that.district) &&
            Objects.equals(division, that.division) &&
            Objects.equals(region, that.region) &&
            Objects.equals(state, that.state) &&
            Objects.equals(country, that.country) &&
            Objects.equals(pincode, that.pincode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        description,
        branchType,
        deliveryStatus,
        taluk,
        circle,
        district,
        division,
        region,
        state,
        country,
        pincode
        );
    }

    @Override
    public String toString() {
        return "PostofficeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (branchType != null ? "branchType=" + branchType + ", " : "") +
                (deliveryStatus != null ? "deliveryStatus=" + deliveryStatus + ", " : "") +
                (taluk != null ? "taluk=" + taluk + ", " : "") +
                (circle != null ? "circle=" + circle + ", " : "") +
                (district != null ? "district=" + district + ", " : "") +
                (division != null ? "division=" + division + ", " : "") +
                (region != null ? "region=" + region + ", " : "") +
                (state != null ? "state=" + state + ", " : "") +
                (country != null ? "country=" + country + ", " : "") +
                (pincode != null ? "pincode=" + pincode + ", " : "") +
            "}";
    }

}
