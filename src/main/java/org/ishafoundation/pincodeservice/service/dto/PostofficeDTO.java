package org.ishafoundation.pincodeservice.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Postoffice entity.
 */
public class PostofficeDTO implements Serializable {

    private Long id;

    private String name;

    private String description;

    private String branchType;

    private String deliveryStatus;

    private String taluk;

    private String circle;

    private String district;

    private String division;

    private String region;

    private String state;

    private String country;

    private Integer pincode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBranchType() {
        return branchType;
    }

    public void setBranchType(String branchType) {
        this.branchType = branchType;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getTaluk() {
        return taluk;
    }

    public void setTaluk(String taluk) {
        this.taluk = taluk;
    }

    public String getCircle() {
        return circle;
    }

    public void setCircle(String circle) {
        this.circle = circle;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PostofficeDTO postofficeDTO = (PostofficeDTO) o;
        if (postofficeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), postofficeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PostofficeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", branchType='" + getBranchType() + "'" +
            ", deliveryStatus='" + getDeliveryStatus() + "'" +
            ", taluk='" + getTaluk() + "'" +
            ", circle='" + getCircle() + "'" +
            ", district='" + getDistrict() + "'" +
            ", division='" + getDivision() + "'" +
            ", region='" + getRegion() + "'" +
            ", state='" + getState() + "'" +
            ", country='" + getCountry() + "'" +
            ", pincode=" + getPincode() +
            "}";
    }
}
