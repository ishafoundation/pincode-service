package org.ishafoundation.pincodeservice.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.github.jhipster.service.filter.IntegerFilter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.ishafoundation.pincodeservice.service.PostofficeService;
import org.ishafoundation.pincodeservice.web.rest.errors.BadRequestAlertException;
import org.ishafoundation.pincodeservice.web.rest.util.HeaderUtil;
import org.ishafoundation.pincodeservice.service.dto.PostofficeDTO;
import org.ishafoundation.pincodeservice.service.dto.PostofficeCriteria;
import org.ishafoundation.pincodeservice.service.PostofficeQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * REST controller for managing Postoffice.
 */
@RestController
@RequestMapping("/api")
public class PostofficeResource {

    private final Logger log = LoggerFactory.getLogger(PostofficeResource.class);

    private static final String ENTITY_NAME = "pincodeservicePostoffice";

    private final PostofficeService postofficeService;

    private final PostofficeQueryService postofficeQueryService;

    private RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

    public PostofficeResource(PostofficeService postofficeService, PostofficeQueryService postofficeQueryService) {
        this.postofficeService = postofficeService;
        this.postofficeQueryService = postofficeQueryService;
    }

    /**
     * GET  /postoffices : get all the postoffices.
     *
     * @param pincode the pincode which the requested postoffices should have.
     * @return the ResponseEntity with status 200 (OK) and the list of postoffices in body
     */
    @GetMapping("/postoffices/{pincode}")
    @Timed
    public ResponseEntity<List<PostofficeDTO>> getPostofficesByPincode(@PathVariable Integer pincode) {
        log.debug("REST request to get Postoffices by pincode: {}", pincode);

        PostofficeCriteria criteria = new PostofficeCriteria();

        IntegerFilter pincodeFilter = new IntegerFilter();
        pincodeFilter.setEquals(pincode);
        criteria.setPincode(pincodeFilter);

        List<PostofficeDTO> entityList = postofficeQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    private Set<String> getPincodes() throws URISyntaxException {
        Set<String> pincodes = new HashSet<>();
        Reader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get("/pincodes.csv"));

            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
                .withHeader("Pincode")
                .withIgnoreHeaderCase()
                .withTrim());
            for (CSVRecord csvRecord : csvParser) {
                pincodes.add(csvRecord.get("Pincode"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
        return pincodes;
    }

    @GetMapping("/load")
    public ResponseEntity<LoadResponse> loadPincodeData() throws URISyntaxException {
        ResponseEntity<PostalResponse> response = null;

        Set<String> pincodes = getPincodes();
        if (pincodes == null) {
            return null;
        }

        for (String pincode : pincodes) {
//
//        }
//
//        for (int pincode = 110000; pincode < 1000000; pincode++) {
//            int first2Digits = pincode / 10000;
//            if (first2Digits == 29 || first2Digits == 35 || first2Digits == 54 || first2Digits == 55
//                || first2Digits == 65 || first2Digits == 66 || first2Digits == 86 || first2Digits == 87
//                || first2Digits == 88 || first2Digits == 89) {
//                pincode += 10000;
//                continue;
//            }
            try {
                response = restTemplate.exchange("http://postalpincode.in/api/pincode/" + pincode,
                    HttpMethod.GET, null, PostalResponse.class);

                if (response != null && response.getBody() != null) {
                    List<PostOffice> postOffices = response.getBody().getPostOffice();
                    if (postOffices != null) {
                        for (PostOffice postOffice : postOffices) {
                            PostofficeDTO postofficeDTO = new PostofficeDTO();
                            postofficeDTO.setPincode(Integer.valueOf(pincode));
                            postofficeDTO.setName(postOffice.getName());
                            postofficeDTO.setDescription(postOffice.getDescription());
                            postofficeDTO.setBranchType(postOffice.getBranchType());
                            postofficeDTO.setDeliveryStatus(postOffice.getDeliveryStatus());
                            postofficeDTO.setTaluk(postOffice.getTaluk());
                            postofficeDTO.setCircle(postOffice.getCircle());
                            postofficeDTO.setDistrict(postOffice.getDistrict());
                            postofficeDTO.setDivision(postOffice.getDivision());
                            postofficeDTO.setRegion(postOffice.getRegion());
                            postofficeDTO.setState(postOffice.getState());
                            postofficeDTO.setCountry(postOffice.getCountry());
                            postofficeService.save(postofficeDTO);
                        }
                    }
                }
            } catch (HttpClientErrorException e) {
                log.debug("HttpClientErrorException :" + e);
            }

        }

        return ResponseEntity.ok().body(new LoadResponse());
    }

    /**
     * POST  /postoffices : Create a new postoffice.
     *
     * @param postofficeDTO the postofficeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new postofficeDTO, or with status 400 (Bad Request) if the postoffice has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/postoffices")
    @Timed
    public ResponseEntity<PostofficeDTO> createPostoffice(@RequestBody PostofficeDTO postofficeDTO) throws URISyntaxException {
        log.debug("REST request to save Postoffice : {}", postofficeDTO);
        if (postofficeDTO.getId() != null) {
            throw new BadRequestAlertException("A new postoffice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PostofficeDTO result = postofficeService.save(postofficeDTO);
        return ResponseEntity.created(new URI("/api/postoffices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /postoffices : Updates an existing postoffice.
     *
     * @param postofficeDTO the postofficeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated postofficeDTO,
     * or with status 400 (Bad Request) if the postofficeDTO is not valid,
     * or with status 500 (Internal Server Error) if the postofficeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/postoffices")
    @Timed
    public ResponseEntity<PostofficeDTO> updatePostoffice(@RequestBody PostofficeDTO postofficeDTO) throws URISyntaxException {
        log.debug("REST request to update Postoffice : {}", postofficeDTO);
        if (postofficeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PostofficeDTO result = postofficeService.save(postofficeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, postofficeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /postoffices : get all the postoffices.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of postoffices in body
     */
    @GetMapping("/postoffices")
    @Timed
    public ResponseEntity<List<PostofficeDTO>> getAllPostoffices(PostofficeCriteria criteria) {
        log.debug("REST request to get Postoffices by criteria: {}", criteria);
//        PostofficeCriteria criteria = new PostofficeCriteria();
//        criteria.setPincode();
        List<PostofficeDTO> entityList = postofficeQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * GET  /postoffices/count : count all the postoffices.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/postoffices/count")
    @Timed
    public ResponseEntity<Long> countPostoffices (PostofficeCriteria criteria) {
        log.debug("REST request to count Postoffices by criteria: {}", criteria);
        return ResponseEntity.ok().body(postofficeQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /postoffices/:id : get the "id" postoffice.
     *
     * @param id the id of the postofficeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the postofficeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/postoffices/{id}")
    @Timed
    public ResponseEntity<PostofficeDTO> getPostoffice(@PathVariable Long id) {
        log.debug("REST request to get Postoffice : {}", id);
        Optional<PostofficeDTO> postofficeDTO = postofficeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(postofficeDTO);
    }

    /**
     * DELETE  /postoffices/:id : delete the "id" postoffice.
     *
     * @param id the id of the postofficeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/postoffices/{id}")
    @Timed
    public ResponseEntity<Void> deletePostoffice(@PathVariable Long id) {
        log.debug("REST request to delete Postoffice : {}", id);
        postofficeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    private static class PostalResponse implements Serializable {
        @JsonProperty("PostOffice")
        private List<PostOffice> postOffice;

        public List<PostofficeResource.PostOffice> getPostOffice() {
            return postOffice;
        }

        public void setPostOffice(List<PostofficeResource.PostOffice> postOffice) {
            this.postOffice = postOffice;
        }
    }

    private static class PostOffice implements Serializable {
        @JsonProperty("Name")
        private String name;
        @JsonProperty("Description")
        private String description;
        @JsonProperty("BranchType")
        private String branchType;
        @JsonProperty("DeliveryStatus")
        private String deliveryStatus;
        @JsonProperty("Taluk")
        private String taluk;
        @JsonProperty("Circle")
        private String circle;
        @JsonProperty("District")
        private String district;
        @JsonProperty("Division")
        private String division;
        @JsonProperty("Region")
        private String region;
        @JsonProperty("State")
        private String state;
        @JsonProperty("Country")
        private String country;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getBranchType() {
            return branchType;
        }

        public void setBranchType(String branchType) {
            this.branchType = branchType;
        }

        public String getDeliveryStatus() {
            return deliveryStatus;
        }

        public void setDeliveryStatus(String deliveryStatus) {
            this.deliveryStatus = deliveryStatus;
        }

        public String getTaluk() {
            return taluk;
        }

        public void setTaluk(String taluk) {
            this.taluk = taluk;
        }

        public String getCircle() {
            return circle;
        }

        public void setCircle(String circle) {
            this.circle = circle;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getDivision() {
            return division;
        }

        public void setDivision(String division) {
            this.division = division;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }
    }

    private static class LoadResponse implements Serializable {
        private String sample;

        public String getSample() {
            return sample;
        }
    }
}
