/**
 * View Models used by Spring MVC REST controllers.
 */
package org.ishafoundation.pincodeservice.web.rest.vm;
