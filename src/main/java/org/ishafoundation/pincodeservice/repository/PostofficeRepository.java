package org.ishafoundation.pincodeservice.repository;

import org.ishafoundation.pincodeservice.domain.Postoffice;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Postoffice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PostofficeRepository extends JpaRepository<Postoffice, Long>, JpaSpecificationExecutor<Postoffice> {

}
